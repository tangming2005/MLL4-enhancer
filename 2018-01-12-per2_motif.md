The repeated Per2 ChIP-seq seems to work, although it is still noisy.

use low p value as cut-off for motif analysis

```bash
 macs14 -t LKR_10_Per2.sorted.bam -c LKR_10_Input.sorted.bam -n Per2 -g mm -p 1e-8
 
 # get the coordinates of 500bp centered on the summit of the Per2 peaks
cat Per2_summits.bed | awk '$2=$2-249, $3=$3+250' OFS="\t" > Per2_500bp_summits.bed

# you need a fasta file of the whole genome, and a bed file containing the cooridnates 
# the whole genome fasta file can be gotten by:
rsync -avzP rsync://hgdownload.cse.ucsc.edu/goldenPath/mm9/chromosomes/ .
cat *fa.gz > UCSC_mm9_genome.fa.gz
gunzip UCSC_mm9_genome.fa.gz

# Use betools get fasta http://bedtools.readthedocs.org/en/latest/content/tools/getfasta.html 
bedtools getfasta -fi UCSC_mm9_genome.fa -bed Per2_500bp_summits.bed -fo Per2_500bp.fa

```

upload the fasta to MEME-ChIP and http://rsat.sb-roscoff.fr/peak-motifs_form.cgi for motif analysis.

`CTCF is in the center of the peak, Sox3 and Foxd3 nearby`


### public data set 

```

Sox3:
https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE33059

https://www.ebi.ac.uk/ena/data/view/SRR354060
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR354/SRR354060/SRR354060.fastq.gz

IgG: 

https://www.ebi.ac.uk/ena/data/view/SRR354069
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR354/SRR354069/SRR354069.fastq.gz

=====================================
https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE58407
https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE70545

HA-Foxd3
https://www.ebi.ac.uk/ena/data/view/SRR2087737
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR208/007/SRR2087737/SRR2087737.fastq.gz

Flag-Foxd3
https://www.ebi.ac.uk/ena/data/view/SRR2087738
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR208/008/SRR2087738/SRR2087738.fastq.gz

Input
https://www.ebi.ac.uk/ena/data/view/SRR2087739
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR208/009/SRR2087739/SRR2087739.fastq.gz

======================================
NSC Foxp1 
https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE101632
https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE62718

NSC Foxp1
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR583/008/SRR5839958/SRR5839958.fastq.gz

NSC Input
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR583/009/SRR5839959/SRR5839959.fastq.gz


```
