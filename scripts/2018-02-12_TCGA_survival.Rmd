---
title: "2018-02-12_TCGA_lung_cancer_clinic_survival"
author: "Ming Tang"
date: "February 12, 2018"
output: html_document
editor_options: 
  chunk_output_type: console
---

### clinical data from RTCGA or TCGAbiolinks are not up to date

use data from https://github.com/OmnesRes/onco_lnc/tree/master/tcga_data instead!

```{r}
setwd("data/TCGA_clinic")
library('TCGAbiolinks')
library('plyr')
library('devtools')
projects <- TCGAbiolinks:::getGDCprojects()$project_id

projects<- c("TCGA-LUAD", "TCGA-LUSC")

clin <- lapply(projects, function(p) {
    message(paste(Sys.time(), 'processing project', p))
    result <- tryCatch({
        query <- GDCquery(project = p, data.category = 'Clinical')
        GDCdownload(query)
        GDCprepare_clinic(query, clinical.info = 'patient')
    }, error = function(e) {
        message(paste0('Error clinical: ', p))
        return(NULL)
    })
    return(result)
})
names(clin) <- projects

## Merge all
clin_all <- rbind.fill(clin)

## Fix columns that have '' that should be NAs
for(j in seq_len(ncol(clin_all))) {
    i <- which(clin_all[, j] == '')
    if(length(i) > 0) clin_all[i, j] <- NA
}

save(clin_all, file = 'clin_all.Rdata')

write.table(clin_all, file = 'clin_all.tsv', quote = FALSE, row.names = FALSE,
    sep = '\t')

```


```{bash}

cd /home/mtang1/projects/MLL4-enhancer/data/TCGA_clinic/onco_linc

wget https://raw.githubusercontent.com/OmnesRes/onco_lnc/master/tcga_data/LUAD/clinical/nationwidechildrens.org_clinical_follow_up_v1.0_luad.txt


```

```{r}
library(tidyverse)
LUAD_clin<- read_tsv("data/TCGA_clinic/onco_linc/nationwidechildrens.org_clinical_follow_up_v1.0_luad.txt", col_names = T)

# get rid of first two rows
LUAD_clin<- LUAD_clin %>% dplyr::slice(-c(1,2)) 
LUAD_clin<- LUAD_clin %>% dplyr::select(bcr_patient_barcode, vital_status, death_days_to, new_tumor_event_dx_days_to, last_contact_days_to) 

head(LUAD_clin)
```

### survival analysis

Start my first survival analysis.

following blog post [here](http://justanotherdatablog.blogspot.com/2015/08/survival-analysis-1.html)  
and [here](http://justanotherdatablog.blogspot.com/2015/08/survival-analysis-2.html)

```{r}
install.packages("OIsurv")
install.packages("survsim")
install.packages("broom")

library("OIsurv")
library("broom")
library(ggplot2)
library(dplyr)
library(survsim)
library(KMsurv)
```

Load example data from the KMsurv package.
use `?tongue` to see the explaination of the dataset 

type
Tumor DNA profile (1=Aneuploid Tumor, 2=Diploid Tumor)

time
Time to death or on-study time, weeks

delta
Death indicator (0=alive, 1=dead)

```{r}
data(tongue)
head(tongue)
```


Just check one type of tumor: 

```{r}
# Analyzing just one type of tumor (Aneuploid Tumor)
tongue2<-tongue %>% filter(type == 1) 

# Converting into a Surv object 
tongue2_surv <- Surv(tongue2$time, tongue2$delta)

# Getting KM estimator
tongue2_survfit<- survfit(tongue2_surv ~ 1)
plot(tongue2_survfit)

```

Graphically Comparing KM estimator for 2 tumors

```{r}

tongue_survfit<- survfit(Surv(time = time, event = delta) ~ type, data = tongue)
plot(tongue_survfit, lty = 2:3, xlab = "weeks", ylab = "Proporation Survival")
legend(100, .8, c("Aneuploid", "Diploid"), lty = 2:3) 
```


ggplot2 more beautiful plots:

```{r}

## tidy the data using broom package
tongue_tidy<- tidy(tongue_survfit)


mx<- max(tongue_tidy$n.censor)

ggplot(tongue_tidy, aes(time, estimate, fill = strata)) + 
  geom_line() +
  geom_point(aes(shape = as.factor(n.censor)), size = 3) + 
  scale_shape_manual(values=c(NA, 1:mx))+
  geom_ribbon(aes(ymin=conf.low, ymax=conf.high), alpha=.25) + 
  xlab("weeks") + 
  ylab("Proportion Survival") + 
        theme_gray(base_size =15)

```

We can compare the survival curves using various statistical tests using survdiff function. Apart from the usual formula and data arguments it takes rho is an argument. Setting different values of rho leads to different kinds of test. In general:  

rho > 0 implies higher weight is given to initial part of the survival curve  
rho < 0 implies higher weight is given to latter part of the survival curve Depending on the value of rho we can get different results as can be seen in the below example.

```{r}
# Statistical tests for comparison
survdiff(Surv(time = time, event = delta) ~ type, data = tongue, rho = 0) # log-rank, default
```

with rho = 1 it is equivalent to the Peto & Peto modification of the Gehan-Wilcoxon test.

```{r}
survdiff(Surv(time = time, event = delta) ~ type, data = tongue, rho = 1) 
```

In my previous post, I went over basics of survival analysis, that included estimating Kaplan-Meier estimate for a given time-to-event data. In this post, I'm exploring on Cox's proportional hazards model for survival data. KM estimator helps in figuring out whether survival function estimates for different groups are same or different. While survival models like Cox's proportional hazards model help in finding relationship between different covariates to the survival function.

Some basic notes about Cox model -

It's a semi-parametric model, as the hazard function (risk per unit time) does not need to be specified.
The proportional hazard condition states that the covariates are multiplicatively related to the hazard. (This assumption should always be checked after fitting a Cox model). 
In case of categorical covariates, the hazard ratio (e.g. treatment vs no treatment) is constant and does not change over time. One can do away with this assumption by using extended Cox model which allows covariates to be dependent on time.
The covariates are constant for each subject and do not vary over time.

(There's one-to-one mapping between hazard function and the survival function i.e. a specific hazard function uniquely determines the survival function and vice versa. Simple mathematical details on this relationship can be found on this wikipedia page.)


###  coxph model with cancer type as covariate.

```{r}
tongue_coxph<- coxph(Surv(time = time, event = delta) ~ as.factor(type), data = tongue)
tongue_coxph_tidy<- tidy(tongue_coxph)
tongue_coxph_tidy # equivalent of print()

```

The confidence interval for the coefficient estimate [-0.083293, 1.0160414] contains 0, implies the relationship is not significant.

The likelihood ratio test against the null model (below) has p-value of 0.1020272 which again supports this.


```{r}
summary(tongue_coxph)

```

### A real example from TCGA SKCM data set

go to [firehose](http://firebrowse.org/?cohort=SKCM&download_dialog=true) SKCM set, download clinical
data by:

`wget http://gdac.broadinstitute.org/runs/stddata__2016_01_28/data/SKCM/20160128/gdac.broadinstitute.org_SKCM.Merge_Clinical.Level_1.2016012800.0.0.tar.gz`

unzip the folder, and the file `SKCM.merged_only_clinical_clin_format.txt` is what we are going to
use.

follow the biostars [post](https://www.biostars.org/p/153013/).

Read in the data:

```{r}
# and read the Clinical file, in this case i transposed it to keep the clinical feature title as column name

## use read_tsv function from readr can sometimes save you a lot of time parsing around...
library(readr)



clinical <- as.data.frame(clin_all)

clinical[1:5,1:5]
rownames(clinical)<- clinical[,1]
clinical<- clinical[,-1]



```

keep only columns we need:  days to death, new tumor event, last day contact to....

```{r}

## this is for disease free survival, if no, just do overall survival.
ind_keep <- grep("days_to_new_tumor_event_after_initial_treatment",colnames(clinical))

# this is a bit tedious, since there are numerous follow ups, let's collapse them together and keep the first value (the higher one) if more than one is available
new_tum <- as.matrix(clinical[,ind_keep])

# do the same to death
ind_keep <- grep("days_to_death",colnames(clinical))
death <- as.matrix(clinical[,ind_keep])

# and days last follow up here we take the most recent which is the max number
ind_keep <- grep("days_to_last_followup",colnames(clinical))

fl <- as.matrix(clinical[,ind_keep])


# and put everything together
all_clin <- data.frame(death, fl)
colnames(all_clin) <- c( "death_days", "followUp_days")

```


```{r}
# create vector with time to new tumor containing data to censor for new_tumor
## somehow the numbers are converted to factors, convert them back
all_clin$new_time <- c()
for (i in 1:length(as.numeric(as.character(all_clin$new_tumor_days)))){
  all_clin$new_time[i] <- ifelse(is.na(as.numeric(as.character(all_clin$new_tumor_days))[i]),
                    as.numeric(as.character(all_clin$followUp_days))[i],as.numeric(as.character(all_clin$new_tumor_days))[i])
}

# create vector time to death containing values to censor for death
all_clin$new_death <- c()
for (i in 1:length(all_clin$death_days)){
  all_clin$new_death[i] <- ifelse(is.na(all_clin$death_days)[i],
                                 all_clin$followUp_days[i], all_clin$death_days[i])
}

```

```{r}
table(clinical$vital_status)

all_clin$death_event <- ifelse(clinical$vital_status == "Alive", 0,1)

#finally add row.names to clinical
rownames(all_clin) <- rownames(clinical)
all_clin$ID<- rownames(clinical)
```


### according to KMT2D expression, split to two groups


```{r}

LUADMatrix_WT %>% head()

## convert to TPM
source("https://bioconductor.org/biocLite.R")
#biocLite("EnsDb.Hsapiens.v83")

#create a tx2gene.txt table
library(EnsDb.Hsapiens.v86)
edb <- EnsDb.Hsapiens.v86

genes_ensemble <- genes(edb)

gene_length<- as.data.frame(genes_ensemble) %>% dplyr::select(gene_id, gene_name, width)


gene_length_in_mat<- left_join(data.frame(gene_id = rownames(LUADMatrix_WT)), gene_length) %>% dplyr::filter(!is.na(width))

LUADMatrix_WT_mat_sub<- LUADMatrix_WT[rownames(LUADMatrix_WT) %in% gene_length_in_mat$gene_id, ]
## now, just use gene_length instead of effective length. effective length = gene length - fragment_length

all.equal(rownames(LUADMatrix_WT), gene_length_in_mat$gene_id)

countToTpm <- function(counts, effLen)
{
    rate <- log(counts + 1) - log(effLen)
    denom <- log(sum(exp(rate)))
    exp(rate - denom + log(1e6))
}


LUADMatrix_WT_TPM<- apply(LUADMatrix_WT_mat_sub, 2, countToTpm, effLen = gene_length_in_mat$width)

head(LUADMatrix_WT_TPM)

hist(log2(LUADMatrix_WT_TPM))

## top and low percentile

cutoffs<- 0.2

## only the samples have survival data. and remove the normal samples 

colnames(LUADMatrix_WT_TPM) %>% str_sub(14, 16) %>% table()
## the samples are subseted by WT status of KMT2D, those are all tumors, no normals

LUAD_WT_TPM_sub<- LUADMatrix_WT_TPM[colnames(LUADMatrix_WT_TPM) %>% str_sub(1, 12) %in% LUAD_clin$bcr_patient_barcode &
                                      colnames(LUADMatrix_WT_TPM) %>% str_sub(14, 15) != "11",]

## KMT2D
gene_to_check<-  LUAD_WT_TPM_sub["ENSG00000167548", ]

gene_to_check<- data.frame(ID = names(gene_to_check) %>% str_sub(1,12), TPM = gene_to_check)
```


```{r}

LUAD_clin_RNA<- inner_join(LUAD_clin, gene_to_check, by = c("bcr_patient_barcode" = "ID")) %>%
  dplyr::filter(vital_status != "[Not Available]")

cutoffs<- 0.2
qts<- quantile(LUAD_clin_RNA$TPM, prob = c(cutoffs, 1- cutoffs))

LUAD_clin_RNA_sub<- LUAD_clin_RNA %>%
  mutate(new_death = case_when(
    death_days_to == "[Not Applicable]" ~ last_contact_days_to,
    TRUE ~ death_days_to
  )) %>% mutate(status = case_when(
    vital_status == "Alive" ~ 0,
    vital_status == "Dead" ~ 1
  )) %>%
  mutate(subtype = case_when(
    TPM <= qts[1] ~ "low",
    TPM >= qts[2] ~ "high",
    TRUE ~ "medium"
  )) %>% 
  dplyr::filter(subtype != "medium") %>%
  mutate(new_death = as.numeric(new_death))

```

survival analysis 

```{r}
library(survival)
library(broom)
LUAD_survfit<- survfit(Surv(time = new_death, event = status) ~ subtype, data = LUAD_clin_RNA_sub)
LUAD_tidy<- tidy(LUAD_survfit)

##logrank test, default
survdiff(Surv(time = new_death, event = status) ~ subtype, data =  LUAD_clin_RNA_sub, rho = 0)


ggplot(SKCM_tidy, aes(time, estimate, fill= strata)) + 
               geom_line(aes(color=strata)) +
               geom_point(aes(shape = as.factor(n.censor)), size = 3) + 
               scale_shape_manual(values=c(16,3), name="censor or not") +
               geom_ribbon(aes(ymin=conf.low, ymax=conf.high), alpha=.25) + 
                xlab("days") + 
                ylab("Proportion Survival") + 
                annotate ("text", x= 6000, y=0.25,label="log-rank p =0.0834\n Gehan-Wilcoxon test p=0.0311") +
               theme_gray(base_size =15)
       
plot(SKCM_survfit, lty = 2:4, xlab = "days", ylab = "Proporation Survival")
legend(100, .8, c("Subgroup1", "Subgroup2", "subgroup3"), lty = 2:4)   

#https://github.com/kassambara/survminer
library("survminer")

ggsurvplot(LUAD_survfit, 
           conf.int = F, # Add confidence interval
           pval = TRUE, 
           ggtheme = theme_gray(base_size=15))

ggsurvplot(SKCM_survfit, 
           conf.int = F, # Add confidence interval
           pval = TRUE, 
           )
```


```{r}

survdiff(Surv(time = new_death, event = death_event) ~ subtype, data = subset_clin, rho = 0) 
survdiff(Surv(time = new_death, event = death_event) ~ subtype, data = subset_clin, rho = 1) 

```


### use RTCGA package


```{r}
source("https://bioconductor.org/biocLite.R")
biocLite("RTCGA.rnaseq")
biocLite("RTCGA.clinical")

library(RTCGA)
library(RTCGA.rnaseq)
library(RTCGA.clinical)


clin_LUAD <- survivalTCGA(LUAD.clinical, 
                     extract.cols="admin.disease_code")
```


```{r}
?mRNA
LUAD.rnaseq[1:5, 1:5]

colnames(LUAD.rnaseq) %>% str_subset("MLL2") 
## ENTREZ ID 8085 is KMT2D. in human it is called MLL2 as well!

## get rid of the normals, 11B
LUAD.rnaseq$bcr_patient_barcode %>% str_sub(14,16) %>% table() 

LUAD_KMT2D_expr<- LUAD.rnaseq %>% 
  # then make it a tibble (nice printing while debugging)
  as_tibble() %>% 
  # then get just a few genes
  dplyr::filter((bcr_patient_barcode %>% str_sub(14,15)) != "11") %>%
  dplyr::select(bcr_patient_barcode, "MLL2|8085") %>% 
  # then trim the barcode (see head(clin), and ?substr)
  mutate(bcr_patient_barcode = substr(bcr_patient_barcode, 1, 12)) %>% 
  # then join back to clinical data
  inner_join(LUAD_clin, by="bcr_patient_barcode") %>%
  dplyr::rename(KMT2D = `MLL2|8085`)

cutoffs<- 0.2

qts<- quantile(LUAD_KMT2D_expr$KMT2D, probs = c(cutoffs, 1- cutoffs)) %>% as.numeric()

LUAD_survival_input<- LUAD_KMT2D_expr %>% 
  dplyr::filter(bcr_patient_barcode %in% (LUAD_KMT2D_WT_samples %>% str_sub(1,12))) %>%
  mutate(subtype = case_when(
    KMT2D <= qts[1] ~ "Low",
    KMT2D >= qts[2] ~  "High",
    TRUE ~ "medium"
  )) %>% 
  mutate(new_death = case_when(
    death_days_to == "[Not Applicable]" ~ last_contact_days_to,
    TRUE ~ death_days_to
  )) %>% mutate(status = case_when(
    vital_status == "Alive" ~ 0,
    vital_status == "Dead" ~ 1
  )) %>% 
  mutate(new_death = as.numeric(new_death))


LUAD_survival_input<- LUAD_survival_input %>% dplyr::filter(subtype != "medium")

library(survival)
library(broom)
LUAD_survfit<- survfit(Surv(time = new_death, event = status) ~ subtype, data = LUAD_survival_input)
LUAD_tidy<- tidy(LUAD_survfit)

##logrank test, default
survdiff(Surv(time = new_death, event = status) ~ subtype, data = LUAD_survival_input, rho = 0)

ggsurvplot(LUAD_survfit, 
           conf.int = F, # Add confidence interval
           pval = TRUE, 
           )
```








