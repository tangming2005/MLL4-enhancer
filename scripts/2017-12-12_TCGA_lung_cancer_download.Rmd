---
title: "2017-12-12_TCGA_lung_cancer_download"
author: "Ming Tang"
date: "December 12, 2017"
output: github_document
editor_options: 
  chunk_output_type: console
---

```{r}
devtools::install_github(repo = "BioinformaticsFMRP/TCGAbiolinks")
library(here)
library(TCGAbiolinks)
library(SummarizedExperiment)
library(tidyverse)
```


### download the mutation file

```{r}


LUSC_maf_hg19_query <- GDCquery(project = "TCGA-LUSC", 
                           data.category = "Simple nucleotide variation", 
                           data.type = "Simple somatic mutation",
                           access = "open", 
                           legacy = TRUE)

LUAD_maf_hg19_query <- GDCquery(project = "TCGA-LUAD", 
                           data.category = "Simple nucleotide variation", 
                           data.type = "Simple somatic mutation",
                           access = "open", 
                           legacy = TRUE)
# Check maf availables
knitr::kable(getResults(LUSC_maf_hg19_query)[,c("created_datetime","file_name")]) 

setwd(here("data/LUSC_mut"))
GDCdownload(LUSC_maf_hg19_query, method = "client")
## this is working
LUSC_maf_hg19 <- GDCprepare(LUSC_maf_hg19_query)


setwd(here("data/LUAD_mut"))
GDCdownload(LUAD_maf_hg19_query, method = "client")
LUAD_maf_hg19 <- GDCprepare(LUAD_maf_hg19_query)


```


There are multiple maf files for each cancer type. I only used 

`gsc_LUAD_pairs.aggregated.capture.tcga.uuid.automated.somatic.maf` and

`gsc_LUSC_pairs.aggregated.capture.tcga.uuid.automated.somatic.maf` which contain 632 samples and 549 samples, respectively.


some tumors have mulitple normal control, and that's why the same tumor has the same mutation replicated 

```{bash}
 cat gsc_LUAD_pairs.aggregated.capture.tcga.uuid.automated.somatic.maf | grep -v "#" | cut -f5-6,11,13,16 |body  sort | uniq > LUAD_oncodrive_input.tsv

 cat gsc_LUSC_pairs.aggregated.capture.tcga.uuid.automated.somatic.maf | grep -v "#" | cut -f5-6,11,13,16 |body  sort | uniq > LUSC_oncodrive_input.tsv

# this is one line contains "Binary" from the maf file. get rid of them mannually 

awk 'FNR==1 && NR!=1{next;}{print}' *tsv1 > NSCLC_oncodirve_input.tsv

```

change the header name according to `oncodrivefml` format.

>Input file format

The variants file is a text file with, at least, 5 columns separated by a tab character (the header is required, but the order of the columns can change):

    Column CHROMOSOME: Chromosome. A number between 1 and 22 or the letter X or Y (upper case)
    Column POSITION: Mutation position. A positive integer.
    Column REF: Reference allele [1].
    Column ALT: Alternate allele [1].
    Column SAMPLE: Sample identifier. Any alphanumeric string.
    Column CANCER_TYPE: Cancer type. Any alphanumeric string. Optional.
    Column SIGNATURE: User defined signature categories. Any alphanumeric string. Optional.

http://oncodrivefml.readthedocs.io/en/latest/files.html#files-input-format

```{bash}
oncodrivefml -i LUAD_oncodrive_input.tsv1 -e cds.regions.gz -c oncodrivefml.conf --type coding --sequencing wes -o LUAD_oncodrive

oncodrivefml -i LUSC_oncodrive_input.tsv1 -e cds.regions.gz -c oncodrivefml.conf --type coding --sequencing wes -o LUSC_oncodrive

oncodrivefml -i NSCLC_oncodirve_input.tsv -e cds.regions.gz -c oncodrivefml.conf --type coding --sequencing wes -o NSCLC_oncodrive


```


### remake the graph 

```{r}
library(ggrepel)
library(readr)
library(tidyverse)
oncodrive<- read_tsv("data/NSCLC_oncodrive/NSCLC_oncodirve_input-oncodrivefml.tsv", col_name =T)

oncodrive_tidy<- oncodrive %>% mutate(affected_sample_rate= SAMPLES/630) %>%
  dplyr::select(Q_VALUE, P_VALUE, SYMBOL, affected_sample_rate) %>%
  mutate(p_value = -log10(P_VALUE)) %>%
  dplyr::filter(affected_sample_rate >= 0.02) %>% 
  arrange(desc(p_value), desc(affected_sample_rate)) %>%
  mutate(p_rank = row_number())
  


ggplot(oncodrive_tidy, aes(x = p_rank, y = p_value)) + 
  geom_point(alpha = 0.2, color = "red") +
  geom_text_repel(aes(label = ifelse(Q_VALUE <=0.001, as.character(SYMBOL), "")), direction = "y") + 
  theme_bw(base_size = 15) 
  

ggplot(oncodrive_tidy, aes(x = p_rank, y = p_value)) + 
  geom_point(alpha = 0.2, color = "red") +
  geom_text_repel(aes(label = ifelse(Q_VALUE <=0.001 & affected_sample_rate >=0.05, as.character(SYMBOL), "")), direction = "y") + 
  theme_bw(base_size = 15) 


oncodrive_tidy %>% 

ggplot(oncodrive_tidy, aes(x = p_rank, y = p_value)) + 
  geom_point(alpha = 0.5, color = "red") + 
  geom_text_repel(aes(label = ifelse(Q_VALUE <=0.001 & affected_sample_rate >=0.05, as.character(SYMBOL), "")), direction = "y") +
  facet_zoom(xlim = c(1, 20), zoom.data = Q_VALUE <=0.001 & affected_sample_rate >=0.05,
             zoom.size = 0.5) + 
  theme_bw(base_size = 15) +
  xlab("ranking") +
  ylab("-log10(Pvalue)")


```



### zoom in by ggforce

```{r}
#devtools::install_github('thomasp85/ggforce')
library(ggforce)

ggplot(oncodrive_tidy, aes(x = p_rank, y = p_value)) + 
  geom_point(alpha = 0.2, color = "red") +
  
  theme_bw(base_size = 15) 


tibble(x=c(2,3,3,4,1), y=c(6,7,9,9,9)) %>% arrange(x, y) %>% mutate(r1=1:n(), r2=rank(x,y))
```




