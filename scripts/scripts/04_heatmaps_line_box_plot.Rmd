---
title: "04_heatmap_line_plot"
author: "Ming Tang"
date: "November 13, 2017"
output: html_document
---

```bash
cd /rsrch2/genomic_med/krai/hunain_histone_reseq/snakemake_ChIPseq_pipeline_downsample/08peak_macs1/merged_peaks


# merge peaks (for H3K27me3, use the non-model output)
cat ../*F_vs_*nomodel_peaks.bed | sort -k1,1 -k2,2n | bedtools merge -i - -d 3000 > merged_F_peaks.bed

#for H3K4me3
cat ../*E_vs_*macs1_peaks.bed | sort -k1,1 -k2,2n | bedtools merge -i - -d 1000 > merged_E_peaks.bed

#for H3K4me1
cat ../*A_vs_*macs1_peaks.bed | sort -k1,1 -k2,2n | bedtools merge -i - -d 1000 > merged_A_peaks.bed

# for H3K9me3

cat ../*B_vs_*macs1_peaks.bed | sort -k1,1 -k2,2n | bedtools merge -i - -d 3000 > merged_B_peaks.bed

#for H3K27ac
cat ../*C_vs_*macs1_peaks.bed | sort -k1,1 -k2,2n | bedtools merge -i - -d 1000 > merged_C_peaks.bed


# for H3K79me2, gene body plot.
```

### Heatmap

```{r}
library(EnrichedHeatmap)
library(rtracklayer)
library(GenomicFeatures)

getCompositeMatrix<- function(target, width, ...){
        signals<- as.list(...)
        signal.names<- lapply(signals, basename)
        # assume those are bigwig files, and extract sample names before .bw
        sample.names<- gsub("_F.bw", "", unlist(signal.names))
        target<- import(target, format = "BED")
        include.bed<- resize(target, width = width, fix = "center")
        bigwigs<- lapply(signals, import, format = "bigWig", which = include.bed)
        mats<- lapply(bigwigs, normalizeToMatrix, 
                      target = resize(include.bed, width = 1, fix = "center"),
                      value_column = "score",
                      # only for broad peaks I set w = 1000, sharp peaks w = 100
                      mean_mode="w0", w=1000, extend = 1/2*width )
        names(mats)<- sample.names
        names(bigwigs)<- sample.names
        return (list(mats = mats, bigwigs = bigwigs))
}


bigwigs<- list.files("data/07bigwig", pattern= "*F.bw", full.names = T)

#bigwigs<- c("data/07bigwig/RAS_mean_E.bw", "data/07bigwig/MLL_1576_E.bw")

mats_bw<- getCompositeMatrix(target = "data/ChIPseq/peaks/08peak_macs1/merged_peaks/merged_F_peaks.bed", width = 100000, bigwigs)

peaks<-import("data/ChIPseq/peaks/08peak_macs1/merged_peaks/merged_F_peaks.bed", format = "BED")

mats<- mats_bw$mats
bw<- mats_bw$bigwigs


lapply(mats, quantile, probs = c(0.005, 0.5,0.95))

mat_merge<- do.call(cbind, mats)
set.seed(100)
km<-kmeans(mat_merge, centers =3)$cluster

col_fun<- circlize::colorRamp2(c(0,20), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 15),
              heatmap_legend_title_gp = gpar(fontsize = 15),
              heatmap_legend_labels_gp = gpar(fontsize = 15))


pdf("results/H3K27me3_50kb.pdf", width = 8, height = 8)

EnrichedHeatmap(mats$RAS_1475, axis_name_rot = 0, name = "RAS_1475", 
                column_title = "RAS_1475", use_raster = TRUE,
                col = col_fun,
                #split = km, cluster_rows = TRUE,
                axis_name_gp = gpar(fontsize = 14), axis_name = c("-50kb", "center", "50kb"),
                heatmap_legend_param = list(color_bar = "discrete")) +
        EnrichedHeatmap(mats$RAS_1508, axis_name_rot = 0, name = "RAS_1508", 
                        column_title ="RAS_1508", use_raster = TRUE,
                        col = col_fun,
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-50kb", "center", "50kb"),
                        heatmap_legend_param = list(color_bar = "discrete")) +
        EnrichedHeatmap(mats$MLL_1527, axis_name_rot = 0, name = "MLL1527", 
                        column_title = "MLL1527", use_raster = TRUE,
                        col = col_fun,
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-50kb", "center", "50kb"),
                        heatmap_legend_param = list(color_bar = "discrete")) +
        EnrichedHeatmap(mats$MLL_1576, axis_name_rot = 0, name = "MLL_1576", 
                        column_title = "MLL_1576", use_raster = TRUE,
                        col = col_fun,
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-50kb", "center", "50kb"),
                        heatmap_legend_param = list(color_bar = "discrete"))

dev.off()


## for H3K4me3 only used one bigwig from MLL KO
pdf("results/H3K4me3_heatmap_RAS_mean_vs_MLL1576.pdf", width = 8, height = 8)

EnrichedHeatmap(mats$RAS, axis_name_rot = 0, name = "RAS", 
                column_title = "RAS", use_raster = TRUE,
                col = col_fun,
                axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "center", "5kb"),
                heatmap_legend_param = list(color_bar = "discrete")) +
        EnrichedHeatmap(mats$MLL, axis_name_rot = 0, name = "MLL", 
                        column_title ="MLL", use_raster = TRUE,
                        col = col_fun,
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "center", "5kb"),
                        heatmap_legend_param = list(color_bar = "discrete")) 

dev.off()

```

### line graph


```{r}


bind_rows(lapply(mats, colMeans))
bind_cols(lapply(mats, colMeans))

RAS_mean<- colMeans(mats$RAS)
RAS_1475_mean<- colMeans(mats$RAS_1475)

all<- bind_cols(lapply(mats, colMeans)) %>% 
  mutate(pos = names(RAS_1475_mean)) %>%
        gather(sample, value, 1:4) 

all<- bind_cols(lapply(mats, colMeans)) %>% 
  mutate(pos = names(RAS_mean)) %>%
        gather(sample, value, 1:2) 

all$pos<- factor(all$pos, levels= names(RAS_1475_mean))
all$pos<- factor(all$pos, levels= names(RAS_mean))

pdf("results/H3K4me3_line1.pdf", width = 4, height = 4)
ggplot(all, aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample), size = 1) + 
        theme_classic(base_size = 16) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "d1", "d50"), labels =c ("-5kb", "center", "5kb")) +
        xlab(NULL) + 
        ggtitle("H3K4me3 signal")
dev.off()

average<- all %>% separate(sample, c("sample", "replicate"), sep= 3) %>% 
        group_by(pos, sample) %>% 
        summarise(value = mean(value))

pdf("results/H3K27me3_line.pdf", width = 4, height = 4)
ggplot(average, aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample), size = 1) + 
        theme_classic(base_size = 16) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "d1", "d50"), labels =c ("-50kb", "center", "50kb")) +
        xlab(NULL) + 
        ggtitle("H3K27me3 signal")
dev.off()


pdf("results/H3K27me3_lin2.pdf", width = 5, height = 4)
ggplot(all, aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample), size = 1) + 
        theme_classic(base_size = 16) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "d1", "d50"), labels =c ("-50kb", "center", "50kb")) +
        xlab(NULL) + 
        ggtitle("H3K27me3 signal")
dev.off()


```


### box plot

```{r}

bw<- mats_bw$bigwigs

binding_score<- lapply(bw, coverage, weight = "score")

binnedSum <- function(numvar, bins, mcolname)
{
  stopifnot(is(bins, "GRanges"))
  stopifnot(is(numvar, "RleList"))
  stopifnot(identical(seqlevels(bins), names(numvar)))
  bins_per_chrom <- split(ranges(bins), seqnames(bins))
  sums_list <- lapply(names(numvar),
      function(seqname) {
          views <- Views(numvar[[seqname]],
                         bins_per_chrom[[seqname]])
          viewSums(views)
      })
  new_mcol <- unsplit(sums_list, as.factor(seqnames(bins)))
  mcols(bins)[[mcolname]] <- new_mcol
  bins
}

## this function is in the GenomicRanges pacakge!, but bins comes first.

binnedAverage <- function(numvar, bins, mcolname)
{
  stopifnot(is(bins, "GRanges"))
  stopifnot(is(numvar, "RleList"))
  stopifnot(identical(seqlevels(bins), names(numvar)))
  bins_per_chrom <- split(ranges(bins), seqnames(bins))
  sums_list <- lapply(names(numvar),
      function(seqname) {
          views <- Views(numvar[[seqname]],
                         bins_per_chrom[[seqname]])
          viewMeans(views)
      })
  new_mcol <- unsplit(sums_list, as.factor(seqnames(bins)))
  mcols(bins)[[mcolname]] <- new_mcol
  bins
}

names(binding_score$MLL_1527)
seqlevels(peaks)

binding_score$MLL_1527_F[seqlevels(peaks)]

### reorder or subset the binding_score to have the same seqlevels as the peaks.

binding_score<- lapply(binding_score, function(x) x[seqlevels(peaks)])

## calculate the mean value of RPKM for each peak
binding_score_at_peaks<-lapply(binding_score, binnedAverage, peaks, "score")



binding_matrix<- bind_cols(lapply(binding_score_at_peaks, function(x) x$score))

binding_matrix<- binding_matrix %>% 
  gather(sample, value, 1:4) %>% 
  mutate(ID = sample) %>%
  separate(sample, c("treatment", "replicate"))

binding_matrix<- binding_matrix %>% 
  gather(sample, value, 1:2) 

pdf("results/H3K4me3_box.pdf", width = 4, height = 4)
ggplot(binding_matrix, aes(x =sample, y = log2(value))) + 
               geom_boxplot(aes(fill = sample)) +
        theme_classic(base_size = 16) +
        ggtitle("H3K4me3 signal for peaks") +
        ylab("log2 signal")
dev.off()

pdf("results/H3K27me3_box.pdf", width = 4, height = 5)
ggplot(binding_matrix, aes(x =ID, y = log2(value))) + 
               geom_boxplot(aes(fill = treatment)) +
        theme_classic(base_size = 16) +
        theme(axis.text.x=element_text(angle=90,hjust=1)) +
        ggtitle("H3K27me3 signal for peaks") +
        ylab("log2 signal")
dev.off()

t.test(as.data.frame(H3K4me1_enhancer_all)$RAS_1508, as.data.frame(H3K4me1_enhancer_all)$MLL_1576, paired = T)

t.test(binding_matrix$RAS, binding_matrix$MLL, paired = T)

wilcox.test(as.data.frame(H3K4me1_enhancer_all)$RAS_1508, as.data.frame(H3K4me1_enhancer_all)$MLL_1576, paired = T)

```


### gene body plot


```{r}
getCompositeMatrixGenebody<- function(genes, width, ...){
        signals<- as.list(...)
        signal.names<- lapply(signals, basename)
        # assume those are bigwig files, and extract sample names before .bw
        sample.names<- gsub("_D.bw", "", unlist(signal.names))
        bigwigs<- lapply(signals, import, format = "bigWig", which = genes)
        mats<- lapply(bigwigs, normalizeToMatrix, 
                      target = genes,
                      value_column = "score",
                      include_target = T,
                      target_ratio = 0.8,
                      mean_mode="w0", w=100, extend = 1/2*width )
        names(mats)<- sample.names
        names(bigwigs)<- sample.names
        return (list(mats = mats, bigwigs = bigwigs))
}


bigwigs<- list.files("data/07bigwig", pattern= "*D.bw", full.names = T)

#bigwigs<- c("data/07bigwig/RAS_mean_E.bw", "data/07bigwig/MLL_1576_E.bw")
library(TxDb.Mmusculus.UCSC.mm9.knownGene)
genes<- genes(TxDb.Mmusculus.UCSC.mm9.knownGene)

genes<- keepStandardChromosomes(genes, pruning.mode="coarse")
mats_bw<- getCompositeMatrixGenebody(genes = genes, width = 10000, bigwigs)

mats<- mats_bw$mats
bw<- mats_bw$bigwigs


lapply(mats, quantile, probs = c(0.005, 0.5,0.95))

mat_merge<- do.call(cbind, mats)
set.seed(100)
km<-kmeans(mat_merge, centers =3)$cluster

col_fun<- circlize::colorRamp2(c(0,20), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 15),
              heatmap_legend_title_gp = gpar(fontsize = 15),
              heatmap_legend_labels_gp = gpar(fontsize = 15))


pdf("results/H3K79me2_gene_body.pdf", width = 9, height = 8)

EnrichedHeatmap(mats$RAS_1475, axis_name_rot = 0, name = "RAS_1475", 
                column_title = "RAS_1475", use_raster = TRUE,
                col = col_fun,
                #split = km, cluster_rows = TRUE,
                axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "start", "end", "5kb"),
                heatmap_legend_param = list(color_bar = "discrete")) +
        EnrichedHeatmap(mats$RAS_1508, axis_name_rot = 0, name = "RAS_1508", 
                        column_title ="RAS_1508", use_raster = TRUE,
                        col = col_fun,
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "start", "end", "5kb"),
                        heatmap_legend_param = list(color_bar = "discrete")) +
        EnrichedHeatmap(mats$MLL_1527, axis_name_rot = 0, name = "MLL1527", 
                        column_title = "MLL1527", use_raster = TRUE,
                        col = col_fun,
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "start", "end", "5kb"),
                        heatmap_legend_param = list(color_bar = "discrete")) +
        EnrichedHeatmap(mats$MLL_1576, axis_name_rot = 0, name = "MLL_1576", 
                        column_title = "MLL_1576", use_raster = TRUE,
                        col = col_fun,
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "start", "end", "5kb"),
                        heatmap_legend_param = list(color_bar = "discrete"))

dev.off()
```

